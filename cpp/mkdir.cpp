#include "windows.h" // for all files we can call
#include <iostream> // basic i/o

//wont use namespace this time


/*
the windows API function to create a directory

BOOL CreateDirectoryA(
 [in] LPCSTR lpPathName,
 [in, optional ] LPSECURITY_ATTRIBUTES lpSecurityAttributes
);

#1 if lpSecurityAttributes is NULL, default security applies
#2 define LPCSTR directly
*/


int main() {

// define a booleans to hold the result of the call
bool oneDir,otherDir; 

// define LPCSTR for use
LPCSTR sameDir = "sameDir";
LPCSTR newDir = "sameDir/newDir";

//define and make call

oneDir = CreateDirectoryA( sameDir, NULL);
otherDir = CreateDirectoryA( newDir, NULL);


if ( oneDir != FALSE && otherDir != FALSE ) {
	std::cout<<"both dirs created \n";
	}
else { 
	std::cout<<"error:\n"<<GetLastError(); 
	}

if ( oneDir != FALSE ) {
	std::cout<<"verified oneDir\n";
	}
else { std::cout<<"oneDir error\n";
	}

return 0;
}
