// winreg.h , windows.h might cover it
#include "windows.h" // for all files we can call
#include "lmcons.h" //for UNLEN
#include <iostream> // basic i/o
#include <string.h>
using namespace std;

/*
LSTATUS RegCreateKeyExA(
  [in]            HKEY                        hKey,
  [in]            LPCSTR                      lpSubKey,
                  DWORD                       Reserved,
  [in, optional]  LPSTR                       lpClass,
  [in]            DWORD                       dwOptions,
  [in]            REGSAM                      samDesired,
  [in, optional]  const LPSECURITY_ATTRIBUTES lpSecurityAttributes,
  [out]           PHKEY                       phkResult,
  [out, optional] LPDWORD                     lpdwDisposition
);

*/



int main() {

// define a booleans to hold the result of the calls
bool user,myDir; 

// define variables 
string homeC = "C:\\Users\\";
string addOn = "\\Desktop\\newfolder";
DWORD size = UNLEN + 1;
char uname[UNLEN + 1];

//define and make calls

//get username
user = GetUserNameA( uname, &size );
int usize = sizeof(uname)/sizeof(char) ;
//for holding new strings to convert
LPTSTR nameVar = new TCHAR[usize + 1];
//copy username into string
strcpy(nameVar,uname);
//combine folder string and concatenate username
string p4wn = homeC + nameVar + addOn;
LPTSTR hh = new TCHAR[p4wn.size()]; 
strcpy(hh, p4wn.c_str());
// cout<< hh << endl; // if you want to confirm the username works
//final conversion to useable string for directory
LPCSTR finalDir = hh;
//create dir assuming windows 
myDir = CreateDirectoryA( hh, NULL);

if ( myDir != FALSE ) {
	cout<<"verified directory\n";
	}	
else { cout<<"check Desktop, encountered an error\n";
	}

return 0;
}
