/********************************************************************************
*                                                                               *
* FileApi.h -- ApiSet Contract for api-ms-win-core-file-l1                      *
*                                                                               *
* Copyright (c) Microsoft Corporation. All rights reserved.                     *
*                                                                               *
********************************************************************************/

#ifdef _MSC_VER
#pragma once
#endif // _MSC_VER

#ifndef _APISETFILE_
#define _APISETFILE_

#include <apiset.h>
#include <apisetcconv.h>
#include <minwindef.h>
#include <minwinbase.h>

#ifdef __cplusplus
extern "C" {
#endif

#pragma region Application Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM)

//
// Constants
//
#define CREATE_NEW          1
#define CREATE_ALWAYS       2
#define OPEN_EXISTING       3
#define OPEN_ALWAYS         4
#define TRUNCATE_EXISTING   5

#define INVALID_FILE_SIZE ((DWORD)0xFFFFFFFF)
#define INVALID_SET_FILE_POINTER ((DWORD)-1)
#define INVALID_FILE_ATTRIBUTES ((DWORD)-1)

#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Application Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
LONG
WINAPI
__stdcall
CompareFileTime(
    _In_ CONST FILETIME* lpFileTime1,
      const FILETIME* lpFileTime1,
    _In_ CONST FILETIME* lpFileTime2
      const FILETIME* lpFileTime2
    );


WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
CreateDirectoryA(
    _In_ LPCSTR lpPathName,
      LPCSTR lpPathName,
    _In_opt_ LPSECURITY_ATTRIBUTES lpSecurityAttributes
      LPSECURITY_ATTRIBUTES lpSecurityAttributes
    );

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
CreateDirectoryW(
    _In_ LPCWSTR lpPathName,
      LPCWSTR lpPathName,
    _In_opt_ LPSECURITY_ATTRIBUTES lpSecurityAttributes
      LPSECURITY_ATTRIBUTES lpSecurityAttributes
    );

#ifdef UNICODE
#define CreateDirectory  CreateDirectoryW
#else
#define CreateDirectory  CreateDirectoryA
#endif // !UNICODE

#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Desktop Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
HANDLE
WINAPI
__stdcall
CreateFileA(
    _In_ LPCSTR lpFileName,
      LPCSTR lpFileName,
    _In_ DWORD dwDesiredAccess,
      DWORD dwDesiredAccess,
    _In_ DWORD dwShareMode,
      DWORD dwShareMode,
    _In_opt_ LPSECURITY_ATTRIBUTES lpSecurityAttributes,
      LPSECURITY_ATTRIBUTES lpSecurityAttributes,
    _In_ DWORD dwCreationDisposition,
      DWORD dwCreationDisposition,
    _In_ DWORD dwFlagsAndAttributes,
      DWORD dwFlagsAndAttributes,
    _In_opt_ HANDLE hTemplateFile
      HANDLE hTemplateFile
    );

WINBASEAPI
__declspec(dllimport)
HANDLE
WINAPI
__stdcall
CreateFileW(
    _In_ LPCWSTR lpFileName,
      LPCWSTR lpFileName,
    _In_ DWORD dwDesiredAccess,
      DWORD dwDesiredAccess,
    _In_ DWORD dwShareMode,
      DWORD dwShareMode,
    _In_opt_ LPSECURITY_ATTRIBUTES lpSecurityAttributes,
      LPSECURITY_ATTRIBUTES lpSecurityAttributes,
    _In_ DWORD dwCreationDisposition,
      DWORD dwCreationDisposition,
    _In_ DWORD dwFlagsAndAttributes,
      DWORD dwFlagsAndAttributes,
    _In_opt_ HANDLE hTemplateFile
      HANDLE hTemplateFile
    );

#ifdef UNICODE
#define CreateFile  CreateFileW
#else
#define CreateFile  CreateFileA
#endif // !UNICODE

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
DefineDosDeviceW(
    _In_ DWORD dwFlags,
      DWORD dwFlags,
    _In_ LPCWSTR lpDeviceName,
      LPCWSTR lpDeviceName,
    _In_opt_ LPCWSTR lpTargetPath
      LPCWSTR lpTargetPath
    );


#ifdef UNICODE
#define DefineDosDevice  DefineDosDeviceW
#endif

#endif // WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM)
#pragma endregion

#pragma region Application Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
DeleteFileA(
    _In_ LPCSTR lpFileName
      LPCSTR lpFileName
    );

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
DeleteFileW(
    _In_ LPCWSTR lpFileName
      LPCWSTR lpFileName
    );

#ifdef UNICODE
#define DeleteFile  DeleteFileW
#else
#define DeleteFile  DeleteFileA
#endif // !UNICODE

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
DeleteVolumeMountPointW(
    _In_ LPCWSTR lpszVolumeMountPoint
      LPCWSTR lpszVolumeMountPoint
    );


#ifdef UNICODE
#define DeleteVolumeMountPoint  DeleteVolumeMountPointW
#endif

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
FileTimeToLocalFileTime(
    _In_ CONST FILETIME* lpFileTime,
      const FILETIME* lpFileTime,
    _Out_ LPFILETIME lpLocalFileTime
      LPFILETIME lpLocalFileTime
    );



WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
FindClose(
    _Inout_ HANDLE hFindFile
      HANDLE hFindFile
    );


#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Desktop Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
FindCloseChangeNotification(
    _In_ HANDLE hChangeHandle
      HANDLE hChangeHandle
    );


WINBASEAPI
__declspec(dllimport)
HANDLE
WINAPI
__stdcall
FindFirstChangeNotificationA(
    _In_ LPCSTR lpPathName,
      LPCSTR lpPathName,
    _In_ BOOL bWatchSubtree,
      BOOL bWatchSubtree,
    _In_ DWORD dwNotifyFilter
      DWORD dwNotifyFilter
    );

WINBASEAPI
__declspec(dllimport)
HANDLE
WINAPI
__stdcall
FindFirstChangeNotificationW(
    _In_ LPCWSTR lpPathName,
      LPCWSTR lpPathName,
    _In_ BOOL bWatchSubtree,
      BOOL bWatchSubtree,
    _In_ DWORD dwNotifyFilter
      DWORD dwNotifyFilter
    );

#ifdef UNICODE
#define FindFirstChangeNotification  FindFirstChangeNotificationW
#else
#define FindFirstChangeNotification  FindFirstChangeNotificationA
#endif // !UNICODE

#endif // WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM)
#pragma endregion

#pragma region Application Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
HANDLE
WINAPI
__stdcall
FindFirstFileA(
    _In_ LPCSTR lpFileName,
      LPCSTR lpFileName,
    _Out_ LPWIN32_FIND_DATAA lpFindFileData
      LPWIN32_FIND_DATAA lpFindFileData
    );

WINBASEAPI
__declspec(dllimport)
HANDLE
WINAPI
__stdcall
FindFirstFileW(
    _In_ LPCWSTR lpFileName,
      LPCWSTR lpFileName,
    _Out_ LPWIN32_FIND_DATAW lpFindFileData
      LPWIN32_FIND_DATAW lpFindFileData
    );

#ifdef UNICODE
#define FindFirstFile  FindFirstFileW
#else
#define FindFirstFile  FindFirstFileA
#endif // !UNICODE

#if (_WIN32_WINNT >= 0x0400)

WINBASEAPI
__declspec(dllimport)
HANDLE
WINAPI
__stdcall
FindFirstFileExA(
    _In_ LPCSTR lpFileName,
      LPCSTR lpFileName,
    _In_ FINDEX_INFO_LEVELS fInfoLevelId,
      FINDEX_INFO_LEVELS fInfoLevelId,
    _Out_writes_bytes_(sizeof(WIN32_FIND_DATAA)) LPVOID lpFindFileData,
      LPVOID lpFindFileData,
    _In_ FINDEX_SEARCH_OPS fSearchOp,
      FINDEX_SEARCH_OPS fSearchOp,
    _Reserved_ LPVOID lpSearchFilter,
      LPVOID lpSearchFilter,
    _In_ DWORD dwAdditionalFlags
      DWORD dwAdditionalFlags
    );

WINBASEAPI
__declspec(dllimport)
HANDLE
WINAPI
__stdcall
FindFirstFileExW(
    _In_ LPCWSTR lpFileName,
      LPCWSTR lpFileName,
    _In_ FINDEX_INFO_LEVELS fInfoLevelId,
      FINDEX_INFO_LEVELS fInfoLevelId,
    _Out_writes_bytes_(sizeof(WIN32_FIND_DATAW)) LPVOID lpFindFileData,
      LPVOID lpFindFileData,
    _In_ FINDEX_SEARCH_OPS fSearchOp,
      FINDEX_SEARCH_OPS fSearchOp,
    _Reserved_ LPVOID lpSearchFilter,
      LPVOID lpSearchFilter,
    _In_ DWORD dwAdditionalFlags
      DWORD dwAdditionalFlags
    );

#ifdef UNICODE
#define FindFirstFileEx  FindFirstFileExW
#else
#define FindFirstFileEx  FindFirstFileExA
#endif // !UNICODE

#endif /* _WIN32_WINNT >= 0x0400 */

#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Desktop Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
HANDLE
WINAPI
__stdcall
FindFirstVolumeW(
    _Out_writes_(cchBufferLength) LPWSTR lpszVolumeName,
      LPWSTR lpszVolumeName,
    _In_ DWORD cchBufferLength
      DWORD cchBufferLength
    );


#ifdef UNICODE
#define FindFirstVolume FindFirstVolumeW
#endif // !UNICODE

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
FindNextChangeNotification(
    _In_ HANDLE hChangeHandle
      HANDLE hChangeHandle
    );


#endif // WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM)
#pragma endregion

#pragma region Application Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
FindNextFileA(
    _In_ HANDLE hFindFile,
      HANDLE hFindFile,
    _Out_ LPWIN32_FIND_DATAA lpFindFileData
      LPWIN32_FIND_DATAA lpFindFileData
    );

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
FindNextFileW(
    _In_ HANDLE hFindFile,
      HANDLE hFindFile,
    _Out_ LPWIN32_FIND_DATAW lpFindFileData
      LPWIN32_FIND_DATAW lpFindFileData
    );

#ifdef UNICODE
#define FindNextFile  FindNextFileW
#else
#define FindNextFile  FindNextFileA
#endif // !UNICODE

#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Desktop Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
FindNextVolumeW(
    _Inout_ HANDLE hFindVolume,
      HANDLE hFindVolume,
    _Out_writes_(cchBufferLength) LPWSTR lpszVolumeName,
      LPWSTR lpszVolumeName,
    _In_ DWORD cchBufferLength
      DWORD cchBufferLength
    );


#ifdef UNICODE
#define FindNextVolume FindNextVolumeW
#endif

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
FindVolumeClose(
    _In_ HANDLE hFindVolume
      HANDLE hFindVolume
    );


#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Application Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
FlushFileBuffers(
    _In_ HANDLE hFile
      HANDLE hFile
    );


#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Application Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
GetDiskFreeSpaceA(
    _In_opt_ LPCSTR lpRootPathName,
      LPCSTR lpRootPathName,
    _Out_opt_ LPDWORD lpSectorsPerCluster,
      LPDWORD lpSectorsPerCluster,
    _Out_opt_ LPDWORD lpBytesPerSector,
      LPDWORD lpBytesPerSector,
    _Out_opt_ LPDWORD lpNumberOfFreeClusters,
      LPDWORD lpNumberOfFreeClusters,
    _Out_opt_ LPDWORD lpTotalNumberOfClusters
      LPDWORD lpTotalNumberOfClusters
    );

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
GetDiskFreeSpaceW(
    _In_opt_ LPCWSTR lpRootPathName,
      LPCWSTR lpRootPathName,
    _Out_opt_ LPDWORD lpSectorsPerCluster,
      LPDWORD lpSectorsPerCluster,
    _Out_opt_ LPDWORD lpBytesPerSector,
      LPDWORD lpBytesPerSector,
    _Out_opt_ LPDWORD lpNumberOfFreeClusters,
      LPDWORD lpNumberOfFreeClusters,
    _Out_opt_ LPDWORD lpTotalNumberOfClusters
      LPDWORD lpTotalNumberOfClusters
    );

#ifdef UNICODE
#define GetDiskFreeSpace  GetDiskFreeSpaceW
#else
#define GetDiskFreeSpace  GetDiskFreeSpaceA
#endif // !UNICODE

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
GetDiskFreeSpaceExA(
    _In_opt_ LPCSTR lpDirectoryName,
      LPCSTR lpDirectoryName,
    _Out_opt_ PULARGE_INTEGER lpFreeBytesAvailableToCaller,
      PULARGE_INTEGER lpFreeBytesAvailableToCaller,
    _Out_opt_ PULARGE_INTEGER lpTotalNumberOfBytes,
      PULARGE_INTEGER lpTotalNumberOfBytes,
    _Out_opt_ PULARGE_INTEGER lpTotalNumberOfFreeBytes
      PULARGE_INTEGER lpTotalNumberOfFreeBytes
    );

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
GetDiskFreeSpaceExW(
    _In_opt_ LPCWSTR lpDirectoryName,
      LPCWSTR lpDirectoryName,
    _Out_opt_ PULARGE_INTEGER lpFreeBytesAvailableToCaller,
      PULARGE_INTEGER lpFreeBytesAvailableToCaller,
    _Out_opt_ PULARGE_INTEGER lpTotalNumberOfBytes,
      PULARGE_INTEGER lpTotalNumberOfBytes,
    _Out_opt_ PULARGE_INTEGER lpTotalNumberOfFreeBytes
      PULARGE_INTEGER lpTotalNumberOfFreeBytes
    );

#ifdef UNICODE
#define GetDiskFreeSpaceEx  GetDiskFreeSpaceExW
#else
#define GetDiskFreeSpaceEx  GetDiskFreeSpaceExA
#endif // !UNICODE

//
//  The structure definition must be same as the one 
//  (FILE_FS_FULL_SIZE_INFORMATION_EX) defined in ntioapi_x.w
//

typedef struct DISK_SPACE_INFORMATION {

    //
    //  AllocationUnits are actually file system clusters.
    //  AllocationUnits * SectorsPerAllocationUnit * BytesPerSector
    //  will get you the sizes in bytes.
    //

    //
    //  The Actual*AllocationUnits are volume sizes without considering Quota
    //  setting.
    //  ActualPoolUnavailableAllocationUnits is the unavailable space for the
    //  volume due to insufficient free pool space (PoolAvailableAllocationUnits).
    //  Be aware AllocationUnits are mesured in clusters, see comments at the beginning.
    //
    //  ActualTotalAllocationUnits = ActualAvailableAllocationUnits +
    //                               ActualPoolUnavailableAllocationUnits +
    //                               UsedAllocationUnits +
    //                               TotalReservedAllocationUnits
    //

    ULONGLONG ActualTotalAllocationUnits;
    ULONGLONG ActualAvailableAllocationUnits;
    ULONGLONG ActualPoolUnavailableAllocationUnits;

    //
    //  The Caller*AllocationUnits are limited by Quota setting.
    //  CallerAvailableAllocationUnits is the unavailable space for the
    //  volume due to insufficient free pool space (PoolAvailableAllocationUnits).
    //  Be aware AllocationUnits are mesured in clusters, see comments at the beginning.
    //
    //  CallerTotalAllocationUnits = CallerAvailableAllocationUnits +
    //                               CallerPoolUnavailableAllocationUnits +
    //                               UsedAllocationUnits +
    //                               TotalReservedAllocationUnits
    //

    ULONGLONG CallerTotalAllocationUnits;
    ULONGLONG CallerAvailableAllocationUnits;
    ULONGLONG CallerPoolUnavailableAllocationUnits;

    //
    //  The used space (in clusters) of the volume.
    //

    ULONGLONG UsedAllocationUnits;

    //
    //  Total reserved space (in clusters).
    //

    ULONGLONG TotalReservedAllocationUnits;

    //
    //  A special type of reserved space (in clusters) for per-volume storage
    //  reserve and this is included in the above TotalReservedAllocationUnits.
    //

    ULONGLONG VolumeStorageReserveAllocationUnits;

    //
    //  This refers to the space (in clusters) that has been committed by
    //  storage pool but has not been allocated by file system.
    //
    //  s1 = (ActualTotalAllocationUnits - UsedAllocationUnits - TotalReservedAllocationUnits)
    //  s2 = (AvailableCommittedAllocationUnits + PoolAvailableAllocationUnits)
    //  ActualAvailableAllocationUnits = min( s1, s2 )
    //
    //  When s1 >= s2, ActualPoolUnavailableAllocationUnits = 0
    //  When s1 < s2, ActualPoolUnavailableAllocationUnits = s2 - s1.
    //

    ULONGLONG AvailableCommittedAllocationUnits;

    //
    //  Available space (in clusters) in corresponding storage pool. If the volume
    //  is not a spaces volume, the PoolAvailableAllocationUnits is set to zero.
    //

    ULONGLONG PoolAvailableAllocationUnits;

    DWORD SectorsPerAllocationUnit;
    DWORD BytesPerSector;

} DISK_SPACE_INFORMATION;

WINBASEAPI
__declspec(dllimport)
HRESULT
WINAPI
__stdcall
GetDiskSpaceInformationA(
    _In_opt_ LPCSTR rootPath,
      LPCSTR rootPath,
    _Out_ DISK_SPACE_INFORMATION* diskSpaceInfo
      DISK_SPACE_INFORMATION* diskSpaceInfo
    );

WINBASEAPI
__declspec(dllimport)
HRESULT
WINAPI
__stdcall
GetDiskSpaceInformationW(
    _In_opt_ LPCWSTR rootPath,
      LPCWSTR rootPath,
    _Out_ DISK_SPACE_INFORMATION* diskSpaceInfo
      DISK_SPACE_INFORMATION* diskSpaceInfo
    );

#ifdef UNICODE
#define GetDiskSpaceInformation  GetDiskSpaceInformationW
#else
#define GetDiskSpaceInformation  GetDiskSpaceInformationA
#endif // !UNICODE

WINBASEAPI
__declspec(dllimport)
UINT
WINAPI
__stdcall
GetDriveTypeA(
    _In_opt_ LPCSTR lpRootPathName
      LPCSTR lpRootPathName
    );

WINBASEAPI
__declspec(dllimport)
UINT
WINAPI
__stdcall
GetDriveTypeW(
    _In_opt_ LPCWSTR lpRootPathName
      LPCWSTR lpRootPathName
    );

#ifdef UNICODE
#define GetDriveType  GetDriveTypeW
#else
#define GetDriveType  GetDriveTypeA
#endif // !UNICODE

#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Application Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM)

typedef struct _WIN32_FILE_ATTRIBUTE_DATA {
    DWORD dwFileAttributes;
    FILETIME ftCreationTime;
    FILETIME ftLastAccessTime;
    FILETIME ftLastWriteTime;
    DWORD nFileSizeHigh;
    DWORD nFileSizeLow;
} WIN32_FILE_ATTRIBUTE_DATA, *LPWIN32_FILE_ATTRIBUTE_DATA;

WINBASEAPI
__declspec(dllimport)
DWORD
WINAPI
__stdcall
GetFileAttributesA(
    _In_ LPCSTR lpFileName
      LPCSTR lpFileName
    );

WINBASEAPI
__declspec(dllimport)
DWORD
WINAPI
__stdcall
GetFileAttributesW(
    _In_ LPCWSTR lpFileName
      LPCWSTR lpFileName
    );

#ifdef UNICODE
#define GetFileAttributes  GetFileAttributesW
#else
#define GetFileAttributes  GetFileAttributesA
#endif // !UNICODE

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
GetFileAttributesExA(
    _In_ LPCSTR lpFileName,
      LPCSTR lpFileName,
    _In_ GET_FILEEX_INFO_LEVELS fInfoLevelId,
      GET_FILEEX_INFO_LEVELS fInfoLevelId,
    _Out_writes_bytes_(sizeof(WIN32_FILE_ATTRIBUTE_DATA)) LPVOID lpFileInformation
      LPVOID lpFileInformation
    );

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
GetFileAttributesExW(
    _In_ LPCWSTR lpFileName,
      LPCWSTR lpFileName,
    _In_ GET_FILEEX_INFO_LEVELS fInfoLevelId,
      GET_FILEEX_INFO_LEVELS fInfoLevelId,
    _Out_writes_bytes_(sizeof(WIN32_FILE_ATTRIBUTE_DATA)) LPVOID lpFileInformation
      LPVOID lpFileInformation
    );

#ifdef UNICODE
#define GetFileAttributesEx  GetFileAttributesExW
#else
#define GetFileAttributesEx  GetFileAttributesExA
#endif // !UNICODE

#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Desktop Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM)

typedef struct _BY_HANDLE_FILE_INFORMATION {
    DWORD dwFileAttributes;
    FILETIME ftCreationTime;
    FILETIME ftLastAccessTime;
    FILETIME ftLastWriteTime;
    DWORD dwVolumeSerialNumber;
    DWORD nFileSizeHigh;
    DWORD nFileSizeLow;
    DWORD nNumberOfLinks;
    DWORD nFileIndexHigh;
    DWORD nFileIndexLow;
} BY_HANDLE_FILE_INFORMATION, *PBY_HANDLE_FILE_INFORMATION, *LPBY_HANDLE_FILE_INFORMATION;

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
GetFileInformationByHandle(
    _In_ HANDLE hFile,
      HANDLE hFile,
    _Out_ LPBY_HANDLE_FILE_INFORMATION lpFileInformation
      LPBY_HANDLE_FILE_INFORMATION lpFileInformation
    );


WINBASEAPI
__declspec(dllimport)
DWORD
WINAPI
__stdcall
GetFileSize(
    _In_ HANDLE hFile,
      HANDLE hFile,
    _Out_opt_ LPDWORD lpFileSizeHigh
      LPDWORD lpFileSizeHigh
    );


#endif // WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM)
#pragma endregion

#pragma region Application Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
GetFileSizeEx(
    _In_ HANDLE hFile,
      HANDLE hFile,
    _Out_ PLARGE_INTEGER lpFileSize
      PLARGE_INTEGER lpFileSize
    );


WINBASEAPI
__declspec(dllimport)
DWORD
WINAPI
__stdcall
GetFileType(
    _In_ HANDLE hFile
      HANDLE hFile
    );


#if (_WIN32_WINNT >= 0x0600)

WINBASEAPI
__declspec(dllimport)
DWORD
WINAPI
__stdcall
GetFinalPathNameByHandleA(
    _In_ HANDLE hFile,
      HANDLE hFile,
    _Out_writes_(cchFilePath) LPSTR lpszFilePath,
      LPSTR lpszFilePath,
    _In_ DWORD cchFilePath,
      DWORD cchFilePath,
    _In_ DWORD dwFlags
      DWORD dwFlags
    );

WINBASEAPI
__declspec(dllimport)
DWORD
WINAPI
__stdcall
GetFinalPathNameByHandleW(
    _In_ HANDLE hFile,
      HANDLE hFile,
    _Out_writes_(cchFilePath) LPWSTR lpszFilePath,
      LPWSTR lpszFilePath,
    _In_ DWORD cchFilePath,
      DWORD cchFilePath,
    _In_ DWORD dwFlags
      DWORD dwFlags
    );

#ifdef UNICODE
#define GetFinalPathNameByHandle  GetFinalPathNameByHandleW
#else
#define GetFinalPathNameByHandle  GetFinalPathNameByHandleA
#endif // !UNICODE

#endif // (_WIN32_WINNT >= 0x0600)

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
GetFileTime(
    _In_ HANDLE hFile,
      HANDLE hFile,
    _Out_opt_ LPFILETIME lpCreationTime,
      LPFILETIME lpCreationTime,
    _Out_opt_ LPFILETIME lpLastAccessTime,
      LPFILETIME lpLastAccessTime,
    _Out_opt_ LPFILETIME lpLastWriteTime
      LPFILETIME lpLastWriteTime
    );


WINBASEAPI
__declspec(dllimport)
_Success_(return != 0 && return < nBufferLength)
DWORD
WINAPI
__stdcall
GetFullPathNameW(
    _In_ LPCWSTR lpFileName,
      LPCWSTR lpFileName,
    _In_ DWORD nBufferLength,
      DWORD nBufferLength,
    _Out_writes_to_opt_(nBufferLength,return + 1) LPWSTR lpBuffer,
      LPWSTR lpBuffer,
    _Outptr_opt_ LPWSTR* lpFilePart
     LPWSTR* lpFilePart
    );


#ifdef UNICODE
#define GetFullPathName  GetFullPathNameW
#endif

WINBASEAPI
__declspec(dllimport)
_Success_(return != 0 && return < nBufferLength)
DWORD
WINAPI
__stdcall
GetFullPathNameA(
    _In_ LPCSTR lpFileName,
      LPCSTR lpFileName,
    _In_ DWORD nBufferLength,
      DWORD nBufferLength,
    _Out_writes_to_opt_(nBufferLength,return + 1) LPSTR lpBuffer,
      LPSTR lpBuffer,
    _Outptr_opt_ LPSTR* lpFilePart
     LPSTR* lpFilePart
    );


#ifndef UNICODE
#define GetFullPathName GetFullPathNameA
#endif

WINBASEAPI
__declspec(dllimport)
DWORD
WINAPI
__stdcall
GetLogicalDrives(
    VOID
    void
    );


#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Desktop Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
DWORD
WINAPI
__stdcall
GetLogicalDriveStringsW(
    _In_ DWORD nBufferLength,
      DWORD nBufferLength,
    _Out_writes_to_opt_(nBufferLength,return + 1) LPWSTR lpBuffer
      LPWSTR lpBuffer
    );


#ifdef UNICODE
#define GetLogicalDriveStrings  GetLogicalDriveStringsW
#endif

WINBASEAPI
__declspec(dllimport)
_Success_(return != 0 && return < cchBuffer)
DWORD
WINAPI
__stdcall
GetLongPathNameA(
    _In_ LPCSTR lpszShortPath,
      LPCSTR lpszShortPath,
    _Out_writes_to_opt_(cchBuffer,return + 1) LPSTR lpszLongPath,
      LPSTR lpszLongPath,
    _In_ DWORD cchBuffer
      DWORD cchBuffer
    );


#ifndef UNICODE
#define GetLongPathName GetLongPathNameA
#endif

#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Application Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
_Success_(return != 0 && return < cchBuffer)
DWORD
WINAPI
__stdcall
GetLongPathNameW(
    _In_ LPCWSTR lpszShortPath,
      LPCWSTR lpszShortPath,
    _Out_writes_to_opt_(cchBuffer,return + 1) LPWSTR lpszLongPath,
      LPWSTR lpszLongPath,
    _In_ DWORD cchBuffer
      DWORD cchBuffer
    );


#ifdef UNICODE
#define GetLongPathName GetLongPathNameW
#endif

#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Desktop Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
_Success_(return != 0 && return < cchBuffer)
DWORD
WINAPI
__stdcall
GetShortPathNameW(
    _In_ LPCWSTR lpszLongPath,
      LPCWSTR lpszLongPath,
    _Out_writes_to_opt_(cchBuffer,return + 1) LPWSTR lpszShortPath,
      LPWSTR lpszShortPath,
    _In_ DWORD cchBuffer
      DWORD cchBuffer
    );


#ifdef UNICODE
#define GetShortPathName  GetShortPathNameW
#endif

#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Application Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
UINT
WINAPI
__stdcall
GetTempFileNameW(
    _In_ LPCWSTR lpPathName,
      LPCWSTR lpPathName,
    _In_ LPCWSTR lpPrefixString,
      LPCWSTR lpPrefixString,
    _In_ UINT uUnique,
      UINT uUnique,
    _Out_writes_(MAX_PATH) LPWSTR lpTempFileName
      LPWSTR lpTempFileName
    );


#ifdef UNICODE
#define GetTempFileName  GetTempFileNameW
#endif

#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Desktop Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM)

#if (_WIN32_WINNT >= 0x0600)

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
GetVolumeInformationByHandleW(
    _In_ HANDLE hFile,
      HANDLE hFile,
    _Out_writes_opt_(nVolumeNameSize) LPWSTR lpVolumeNameBuffer,
      LPWSTR lpVolumeNameBuffer,
    _In_ DWORD nVolumeNameSize,
      DWORD nVolumeNameSize,
    _Out_opt_ LPDWORD lpVolumeSerialNumber,
      LPDWORD lpVolumeSerialNumber,
    _Out_opt_ LPDWORD lpMaximumComponentLength,
      LPDWORD lpMaximumComponentLength,
    _Out_opt_ LPDWORD lpFileSystemFlags,
      LPDWORD lpFileSystemFlags,
    _Out_writes_opt_(nFileSystemNameSize) LPWSTR lpFileSystemNameBuffer,
      LPWSTR lpFileSystemNameBuffer,
    _In_ DWORD nFileSystemNameSize
      DWORD nFileSystemNameSize
    );


#endif /* _WIN32_WINNT >=  0x0600 */
#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Application Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
GetVolumeInformationW(
    _In_opt_ LPCWSTR lpRootPathName,
      LPCWSTR lpRootPathName,
    _Out_writes_opt_(nVolumeNameSize) LPWSTR lpVolumeNameBuffer,
      LPWSTR lpVolumeNameBuffer,
    _In_ DWORD nVolumeNameSize,
      DWORD nVolumeNameSize,
    _Out_opt_ LPDWORD lpVolumeSerialNumber,
      LPDWORD lpVolumeSerialNumber,
    _Out_opt_ LPDWORD lpMaximumComponentLength,
      LPDWORD lpMaximumComponentLength,
    _Out_opt_ LPDWORD lpFileSystemFlags,
      LPDWORD lpFileSystemFlags,
    _Out_writes_opt_(nFileSystemNameSize) LPWSTR lpFileSystemNameBuffer,
      LPWSTR lpFileSystemNameBuffer,
    _In_ DWORD nFileSystemNameSize
      DWORD nFileSystemNameSize
    );


#ifdef UNICODE
#define GetVolumeInformation  GetVolumeInformationW
#endif

#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Desktop Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
GetVolumePathNameW(
    _In_ LPCWSTR lpszFileName,
      LPCWSTR lpszFileName,
    _Out_writes_(cchBufferLength) LPWSTR lpszVolumePathName,
      LPWSTR lpszVolumePathName,
    _In_ DWORD cchBufferLength
      DWORD cchBufferLength
    );


#ifdef UNICODE
#define GetVolumePathName  GetVolumePathNameW
#endif

#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Application Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
LocalFileTimeToFileTime(
    _In_ CONST FILETIME* lpLocalFileTime,
      const FILETIME* lpLocalFileTime,
    _Out_ LPFILETIME lpFileTime
      LPFILETIME lpFileTime
    );


WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
LockFile(
    _In_ HANDLE hFile,
      HANDLE hFile,
    _In_ DWORD dwFileOffsetLow,
      DWORD dwFileOffsetLow,
    _In_ DWORD dwFileOffsetHigh,
      DWORD dwFileOffsetHigh,
    _In_ DWORD nNumberOfBytesToLockLow,
      DWORD nNumberOfBytesToLockLow,
    _In_ DWORD nNumberOfBytesToLockHigh
      DWORD nNumberOfBytesToLockHigh
    );


WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
LockFileEx(
    _In_ HANDLE hFile,
      HANDLE hFile,
    _In_ DWORD dwFlags,
      DWORD dwFlags,
    _Reserved_ DWORD dwReserved,
      DWORD dwReserved,
    _In_ DWORD nNumberOfBytesToLockLow,
      DWORD nNumberOfBytesToLockLow,
    _In_ DWORD nNumberOfBytesToLockHigh,
      DWORD nNumberOfBytesToLockHigh,
    _Inout_ LPOVERLAPPED lpOverlapped
      LPOVERLAPPED lpOverlapped
    );


#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Desktop Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
DWORD
WINAPI
__stdcall
QueryDosDeviceW(
    _In_opt_ LPCWSTR lpDeviceName,
      LPCWSTR lpDeviceName,
    _Out_writes_to_opt_(ucchMax,return) LPWSTR lpTargetPath,
      LPWSTR lpTargetPath,
    _In_ DWORD ucchMax
      DWORD ucchMax
    );


#ifdef UNICODE
#define QueryDosDevice  QueryDosDeviceW
#endif

#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Application Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
_Must_inspect_result_
BOOL
WINAPI
__stdcall
ReadFile(
    _In_ HANDLE hFile,
      HANDLE hFile,
    _Out_writes_bytes_to_opt_(nNumberOfBytesToRead, *lpNumberOfBytesRead) __out_data_source(FILE) LPVOID lpBuffer,
       LPVOID lpBuffer,
    _In_ DWORD nNumberOfBytesToRead,
      DWORD nNumberOfBytesToRead,
    _Out_opt_ LPDWORD lpNumberOfBytesRead,
      LPDWORD lpNumberOfBytesRead,
    _Inout_opt_ LPOVERLAPPED lpOverlapped
      LPOVERLAPPED lpOverlapped
    );


WINBASEAPI
__declspec(dllimport)
_Must_inspect_result_
BOOL
WINAPI
__stdcall
ReadFileEx(
    _In_ HANDLE hFile,
      HANDLE hFile,
    _Out_writes_bytes_opt_(nNumberOfBytesToRead) __out_data_source(FILE) LPVOID lpBuffer,
       LPVOID lpBuffer,
    _In_ DWORD nNumberOfBytesToRead,
      DWORD nNumberOfBytesToRead,
    _Inout_ LPOVERLAPPED lpOverlapped,
      LPOVERLAPPED lpOverlapped,
    _In_ LPOVERLAPPED_COMPLETION_ROUTINE lpCompletionRoutine
      LPOVERLAPPED_COMPLETION_ROUTINE lpCompletionRoutine
    );

    
#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Desktop Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
_Must_inspect_result_
BOOL
WINAPI
__stdcall
ReadFileScatter(
    _In_ HANDLE hFile,
      HANDLE hFile,
    _In_ FILE_SEGMENT_ELEMENT aSegmentArray[],
      FILE_SEGMENT_ELEMENT aSegmentArray[],
    _In_ DWORD nNumberOfBytesToRead,
      DWORD nNumberOfBytesToRead,
    _Reserved_ LPDWORD lpReserved,
      LPDWORD lpReserved,
    _Inout_ LPOVERLAPPED lpOverlapped
      LPOVERLAPPED lpOverlapped
    );


#endif // WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM)
#pragma endregion

#pragma region Application Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
RemoveDirectoryA(
    _In_ LPCSTR lpPathName
      LPCSTR lpPathName
    );

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
RemoveDirectoryW(
    _In_ LPCWSTR lpPathName
      LPCWSTR lpPathName
    );

#ifdef UNICODE
#define RemoveDirectory  RemoveDirectoryW
#else
#define RemoveDirectory  RemoveDirectoryA
#endif // !UNICODE

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
SetEndOfFile(
    _In_ HANDLE hFile
      HANDLE hFile
    );


WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
SetFileAttributesA(
    _In_ LPCSTR lpFileName,
      LPCSTR lpFileName,
    _In_ DWORD dwFileAttributes
      DWORD dwFileAttributes
    );

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
SetFileAttributesW(
    _In_ LPCWSTR lpFileName,
      LPCWSTR lpFileName,
    _In_ DWORD dwFileAttributes
      DWORD dwFileAttributes
    );

#ifdef UNICODE
#define SetFileAttributes  SetFileAttributesW
#else
#define SetFileAttributes  SetFileAttributesA
#endif // !UNICODE

#if (_WIN32_WINNT >= 0x0600)

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
SetFileInformationByHandle(
    _In_ HANDLE hFile,
      HANDLE hFile,
    _In_ FILE_INFO_BY_HANDLE_CLASS FileInformationClass,
      FILE_INFO_BY_HANDLE_CLASS FileInformationClass,
    _In_reads_bytes_(dwBufferSize) LPVOID lpFileInformation,
      LPVOID lpFileInformation,
    _In_ DWORD dwBufferSize
      DWORD dwBufferSize
    );


#endif

WINBASEAPI
__declspec(dllimport)
DWORD
WINAPI
__stdcall
SetFilePointer(
    _In_ HANDLE hFile,
      HANDLE hFile,
    _In_ LONG lDistanceToMove,
      LONG lDistanceToMove,
    _Inout_opt_ PLONG lpDistanceToMoveHigh,
      PLONG lpDistanceToMoveHigh,
    _In_ DWORD dwMoveMethod
      DWORD dwMoveMethod
    );


WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
SetFilePointerEx(
    _In_ HANDLE hFile,
      HANDLE hFile,
    _In_ LARGE_INTEGER liDistanceToMove,
      LARGE_INTEGER liDistanceToMove,
    _Out_opt_ PLARGE_INTEGER lpNewFilePointer,
      PLARGE_INTEGER lpNewFilePointer,
    _In_ DWORD dwMoveMethod
      DWORD dwMoveMethod
    );


WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
SetFileTime(
    _In_ HANDLE hFile,
      HANDLE hFile,
    _In_opt_ CONST FILETIME* lpCreationTime,
      const FILETIME* lpCreationTime,
    _In_opt_ CONST FILETIME* lpLastAccessTime,
      const FILETIME* lpLastAccessTime,
    _In_opt_ CONST FILETIME* lpLastWriteTime
      const FILETIME* lpLastWriteTime
    );


#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Desktop Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM)

#if _WIN32_WINNT >= 0x0501

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
SetFileValidData(
    _In_ HANDLE hFile,
      HANDLE hFile,
    _In_ LONGLONG ValidDataLength
      LONGLONG ValidDataLength
    );


#endif // (_WIN32_WINNT >= 0x0501)

#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Application Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
UnlockFile(
    _In_ HANDLE hFile,
      HANDLE hFile,
    _In_ DWORD dwFileOffsetLow,
      DWORD dwFileOffsetLow,
    _In_ DWORD dwFileOffsetHigh,
      DWORD dwFileOffsetHigh,
    _In_ DWORD nNumberOfBytesToUnlockLow,
      DWORD nNumberOfBytesToUnlockLow,
    _In_ DWORD nNumberOfBytesToUnlockHigh
      DWORD nNumberOfBytesToUnlockHigh
    );


WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
UnlockFileEx(
    _In_ HANDLE hFile,
      HANDLE hFile,
    _Reserved_ DWORD dwReserved,
      DWORD dwReserved,
    _In_ DWORD nNumberOfBytesToUnlockLow,
      DWORD nNumberOfBytesToUnlockLow,
    _In_ DWORD nNumberOfBytesToUnlockHigh,
      DWORD nNumberOfBytesToUnlockHigh,
    _Inout_ LPOVERLAPPED lpOverlapped
      LPOVERLAPPED lpOverlapped
    );


WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
WriteFile(
    _In_ HANDLE hFile,
      HANDLE hFile,
    _In_reads_bytes_opt_(nNumberOfBytesToWrite) LPCVOID lpBuffer,
      LPCVOID lpBuffer,
    _In_ DWORD nNumberOfBytesToWrite,
      DWORD nNumberOfBytesToWrite,
    _Out_opt_ LPDWORD lpNumberOfBytesWritten,
      LPDWORD lpNumberOfBytesWritten,
    _Inout_opt_ LPOVERLAPPED lpOverlapped
      LPOVERLAPPED lpOverlapped
    );


WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
WriteFileEx(
    _In_ HANDLE hFile,
      HANDLE hFile,
    _In_reads_bytes_opt_(nNumberOfBytesToWrite) LPCVOID lpBuffer,
      LPCVOID lpBuffer,
    _In_ DWORD nNumberOfBytesToWrite,
      DWORD nNumberOfBytesToWrite,
    _Inout_ LPOVERLAPPED lpOverlapped,
      LPOVERLAPPED lpOverlapped,
    _In_ LPOVERLAPPED_COMPLETION_ROUTINE lpCompletionRoutine
      LPOVERLAPPED_COMPLETION_ROUTINE lpCompletionRoutine
    );

    
#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Desktop Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
WriteFileGather(
    _In_ HANDLE hFile,
      HANDLE hFile,
    _In_ FILE_SEGMENT_ELEMENT aSegmentArray[],
      FILE_SEGMENT_ELEMENT aSegmentArray[],
    _In_ DWORD nNumberOfBytesToWrite,
      DWORD nNumberOfBytesToWrite,
    _Reserved_ LPDWORD lpReserved,
      LPDWORD lpReserved,
    _Inout_ LPOVERLAPPED lpOverlapped
      LPOVERLAPPED lpOverlapped
    );


#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Application Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
DWORD
WINAPI
__stdcall
GetTempPathW(
    _In_ DWORD nBufferLength,
      DWORD nBufferLength,
    _Out_writes_to_opt_(nBufferLength,return + 1) LPWSTR lpBuffer
      LPWSTR lpBuffer
    );


#ifdef UNICODE
#define GetTempPath  GetTempPathW
#endif

#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Desktop Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
GetVolumeNameForVolumeMountPointW(
    _In_ LPCWSTR lpszVolumeMountPoint,
      LPCWSTR lpszVolumeMountPoint,
    _Out_writes_(cchBufferLength) LPWSTR lpszVolumeName,
      LPWSTR lpszVolumeName,
    _In_ DWORD cchBufferLength
      DWORD cchBufferLength
    );


#ifdef UNICODE
#define GetVolumeNameForVolumeMountPoint  GetVolumeNameForVolumeMountPointW
#endif

#if (_WIN32_WINNT >= 0x0501)

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
GetVolumePathNamesForVolumeNameW(
    _In_ LPCWSTR lpszVolumeName,
      LPCWSTR lpszVolumeName,
    _Out_writes_to_opt_(cchBufferLength,*lpcchReturnLength) _Post_ _NullNull_terminated_ LPWCH lpszVolumePathNames,
         LPWCH lpszVolumePathNames,
    _In_ DWORD cchBufferLength,
      DWORD cchBufferLength,
    _Out_ PDWORD lpcchReturnLength
      PDWORD lpcchReturnLength
    );


#ifdef UNICODE
#define GetVolumePathNamesForVolumeName  GetVolumePathNamesForVolumeNameW
#endif

#endif // _WIN32_WINNT >= 0x0501

#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Application Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM)

#if (_WIN32_WINNT >= 0x0602)

typedef struct _CREATEFILE2_EXTENDED_PARAMETERS {
    DWORD dwSize;
    DWORD dwFileAttributes;
    DWORD dwFileFlags;
    DWORD dwSecurityQosFlags;
    LPSECURITY_ATTRIBUTES lpSecurityAttributes;
    HANDLE hTemplateFile;
} CREATEFILE2_EXTENDED_PARAMETERS, *PCREATEFILE2_EXTENDED_PARAMETERS, *LPCREATEFILE2_EXTENDED_PARAMETERS;

WINBASEAPI
__declspec(dllimport)
HANDLE
WINAPI
__stdcall
CreateFile2(
    _In_ LPCWSTR lpFileName,
      LPCWSTR lpFileName,
    _In_ DWORD dwDesiredAccess,
      DWORD dwDesiredAccess,
    _In_ DWORD dwShareMode,
      DWORD dwShareMode,
    _In_ DWORD dwCreationDisposition,
      DWORD dwCreationDisposition,
    _In_opt_ LPCREATEFILE2_EXTENDED_PARAMETERS pCreateExParams
      LPCREATEFILE2_EXTENDED_PARAMETERS pCreateExParams
    );


#endif // _WIN32_WINNT >= 0x0602

#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Desktop Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM)

#if (_WIN32_WINNT >= 0x0600)

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
SetFileIoOverlappedRange(
    _In_ HANDLE FileHandle,
      HANDLE FileHandle,
    _In_ PUCHAR OverlappedRangeStart,
      PUCHAR OverlappedRangeStart,
    _In_ ULONG Length
      ULONG Length
    );


#endif // _WIN32_WINNT >= 0x0600

#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Desktop Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM)

#if _WIN32_WINNT >= 0x0501

WINBASEAPI
__declspec(dllimport)
DWORD
WINAPI
__stdcall
GetCompressedFileSizeA(
    _In_ LPCSTR lpFileName,
      LPCSTR lpFileName,
    _Out_opt_ LPDWORD lpFileSizeHigh
      LPDWORD lpFileSizeHigh
    );

WINBASEAPI
__declspec(dllimport)
DWORD
WINAPI
__stdcall
GetCompressedFileSizeW(
    _In_ LPCWSTR lpFileName,
      LPCWSTR lpFileName,
    _Out_opt_ LPDWORD lpFileSizeHigh
      LPDWORD lpFileSizeHigh
    );

#ifdef UNICODE
#define GetCompressedFileSize  GetCompressedFileSizeW
#else
#define GetCompressedFileSize  GetCompressedFileSizeA
#endif // !UNICODE

#endif // _WIN32_WINNT >= 0x0501

#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Desktop Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM)

#if (_WIN32_WINNT >= 0x0501)

typedef enum _STREAM_INFO_LEVELS {

    FindStreamInfoStandard,
    FindStreamInfoMaxInfoLevel

} STREAM_INFO_LEVELS;

typedef struct _WIN32_FIND_STREAM_DATA {

    LARGE_INTEGER StreamSize;
    WCHAR cStreamName[ MAX_PATH + 36 ];
    WCHAR cStreamName[ 260 + 36 ];

} WIN32_FIND_STREAM_DATA, *PWIN32_FIND_STREAM_DATA;

WINBASEAPI
__declspec(dllimport)
HANDLE
WINAPI
__stdcall
FindFirstStreamW(
    _In_ LPCWSTR lpFileName,
      LPCWSTR lpFileName,
    _In_ STREAM_INFO_LEVELS InfoLevel,
      STREAM_INFO_LEVELS InfoLevel,
    _Out_writes_bytes_(sizeof(WIN32_FIND_STREAM_DATA)) LPVOID lpFindStreamData,
      LPVOID lpFindStreamData,
    _Reserved_ DWORD dwFlags
      DWORD dwFlags
    );


WINBASEAPI
__declspec(dllimport)
BOOL
APIENTRY
__stdcall
FindNextStreamW(
    _In_ HANDLE hFindStream,
      HANDLE hFindStream,
    _Out_writes_bytes_(sizeof(WIN32_FIND_STREAM_DATA)) LPVOID lpFindStreamData
      LPVOID lpFindStreamData
    );


#endif // (_WIN32_WINNT >= 0x0501)

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
AreFileApisANSI(
    VOID
    void
    );


#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Application Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
DWORD
WINAPI
__stdcall
GetTempPathA(
    _In_ DWORD nBufferLength,
      DWORD nBufferLength,
    _Out_writes_to_opt_(nBufferLength,return + 1) LPSTR lpBuffer
      LPSTR lpBuffer
    );


#ifndef UNICODE
#define GetTempPath  GetTempPathA
#endif

#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Desktop Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM)

#if _WIN32_WINNT >= 0x0600

WINBASEAPI
__declspec(dllimport)
HANDLE
WINAPI
__stdcall
FindFirstFileNameW(
    _In_ LPCWSTR lpFileName,
      LPCWSTR lpFileName,
    _In_ DWORD dwFlags,
      DWORD dwFlags,
    _Inout_ LPDWORD StringLength,
      LPDWORD StringLength,
    _Out_writes_(*StringLength) PWSTR LinkName
      PWSTR LinkName
    );


WINBASEAPI
__declspec(dllimport)
BOOL
APIENTRY
__stdcall
FindNextFileNameW(
    _In_ HANDLE hFindStream,
      HANDLE hFindStream,
    _Inout_ LPDWORD StringLength,
      LPDWORD StringLength,
    _Out_writes_(*StringLength) PWSTR LinkName
      PWSTR LinkName
    );


#endif // (_WIN32_WINNT >= 0x0600)

#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Application Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
BOOL
WINAPI
__stdcall
GetVolumeInformationA(
    _In_opt_ LPCSTR lpRootPathName,
      LPCSTR lpRootPathName,
    _Out_writes_opt_(nVolumeNameSize) LPSTR lpVolumeNameBuffer,
      LPSTR lpVolumeNameBuffer,
    _In_ DWORD nVolumeNameSize,
      DWORD nVolumeNameSize,
    _Out_opt_ LPDWORD lpVolumeSerialNumber,
      LPDWORD lpVolumeSerialNumber,
    _Out_opt_ LPDWORD lpMaximumComponentLength,
      LPDWORD lpMaximumComponentLength,
    _Out_opt_ LPDWORD lpFileSystemFlags,
      LPDWORD lpFileSystemFlags,
    _Out_writes_opt_(nFileSystemNameSize) LPSTR lpFileSystemNameBuffer,
      LPSTR lpFileSystemNameBuffer,
    _In_ DWORD nFileSystemNameSize
      DWORD nFileSystemNameSize
    );


#ifndef UNICODE
#define GetVolumeInformation  GetVolumeInformationA
#endif

WINBASEAPI
__declspec(dllimport)
UINT
WINAPI
__stdcall
GetTempFileNameA(
    _In_ LPCSTR lpPathName,
      LPCSTR lpPathName,
    _In_ LPCSTR lpPrefixString,
      LPCSTR lpPrefixString,
    _In_ UINT uUnique,
      UINT uUnique,
    _Out_writes_(MAX_PATH) LPSTR lpTempFileName
      LPSTR lpTempFileName
    );


#ifndef UNICODE
#define GetTempFileName  GetTempFileNameA
#endif
#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_APP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#pragma region Desktop Family or OneCore Family
#if WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM)

WINBASEAPI
__declspec(dllimport)
VOID
void
WINAPI
__stdcall
SetFileApisToOEM(
    VOID
    void
    );


WINBASEAPI
__declspec(dllimport)
VOID
void
WINAPI
__stdcall
SetFileApisToANSI(
    VOID
    void
    );


#endif /* WINAPI_FAMILY_PARTITION(WINAPI_PARTITION_DESKTOP | WINAPI_PARTITION_SYSTEM) */
#pragma endregion

#ifdef __cplusplus
}
#endif

#endif // _APISETFILE_

