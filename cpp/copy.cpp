#include "windows.h"
#include <iostream>
#include <string.h>

/*
BOOL CopyFile(
  [in] LPCTSTR lpExistingFileName,
  [in] LPCTSTR lpNewFileName,
  [in] BOOL    bFailIfExists
);

[in] bFailIfExists
- If this parameter is TRUE and the new file specified by lpNewFileName
	already exists, the function fails. 
- If this parameter is FALSE and the new file already exists, 
	the function overwrites the existing file and succeeds.

*/


int main() {
	
bool isCopied,ifExists;

LPCSTR oldF = "test1.txt";
LPCSTR newF = "test2.txt";
ifExists = TRUE;
	
isCopied = CopyFile(oldF,newF,ifExists);

if (isCopied != FALSE){
	std::cout<<"This worked :) \n";
}
else {
	std::cout<<"This failed!!\n";
}	return 0;
}
