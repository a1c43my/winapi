#include "windows.h" // for all files we can call
#include "lmcons.h" //for UNLEN
#include <iostream> // basic i/o

using namespace std;

/*

the windows API function to get username

BOOL GetUserNameA(
 [out] LPSTR lpBuffer,
 [in, out ] LPDWORD pcbBuffer
);

#1 pointer to buffer for username
#2 buffer and its size

*/


int main() {

// define a booleans to hold the result of the call
bool user; 

// define LPCSTR for use
DWORD size = UNLEN + 1;
char uname[UNLEN + 1];

//define and make call

user = GetUserNameA( uname, &size );

cout<<uname;

return 0;
}
