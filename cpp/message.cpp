 
 /********************************************************************************
*  	MessageBoxA							                                         *
********************************************************************************/
 #include "windows.h"
 #include <iostream>
 #include <string.h>
 
 
 /*
 int MessageBox(
  [in, optional] HWND    hWnd,
  [in, optional] LPCTSTR lpText,
  [in, optional] LPCTSTR lpCaption, 
  [in]           UINT    uType
);

*/

int main() {
	// msgBox int
	int msg,msg2;
	// definitions from WinAPI
	HWND    hWnd;
	LPCTSTR lpText = "WindowName";	// window text
	LPCTSTR lpCaption = "WindowCaption"; //name of window
 	UINT    uType;
 	
 	msg = MessageBox(
        NULL,
        lpText,
        lpCaption,
        MB_ICONASTERISK | MB_YESNO
    );
    
    std::cout<<msg;
    
    if ( msg == 6){
    	msg2 = MessageBox(
        NULL,
        "You clicked Yes",
        "Accepted Window",
        MB_ICONASTERISK | MB_OK
    );
	}
    if ( msg == 7){
    	msg2 = MessageBox(
        NULL,
        "You clicked No",
        "Denial Window",
        MB_ICONASTERISK | MB_OK
    );	
	}
  
    
    
	return 0;
}
